<?php

/**
 * @file
 * uw_auth_required.features.inc
 */

/**
 * Implements hook_user_default_permissions_alter().
 */
function uw_auth_required_user_default_permissions_alter(&$data) {
  if (isset($data['access content'])) {
    unset($data['access content']['roles']['anonymous user']);
  }
}

/**
 * Implements hook_strongarm_alter().
 */
function uw_auth_required_strongarm_alter(&$data) {
  if (isset($data['cas_changePasswordURL'])) {
    $data['cas_changePasswordURL']->value = ''; /* WAS: 'https://watiam.uwaterloo.ca/idm/user/login.jsp' */
  }
  if (isset($data['cas_login_drupal_invite'])) {
    $data['cas_login_drupal_invite']->value = 'Log in'; /* WAS: 'Log in locally (no single sign-on)' */
  }
  if (isset($data['cas_login_form'])) {
    $data['cas_login_form']->value = 0; /* WAS: 2 */
  }
  if (isset($data['service_links_category_vocs'])) {
    $data['service_links_category_vocs']->value['12'] = 0; /* WAS: '' */
    $data['service_links_category_vocs']->value['13'] = 0; /* WAS: '' */
    unset($data['service_links_category_vocs']->value['2']);
    unset($data['service_links_category_vocs']->value['3']);
    unset($data['service_links_category_vocs']->value['4']);
    unset($data['service_links_category_vocs']->value['5']);
    unset($data['service_links_category_vocs']->value['6']);
    unset($data['service_links_category_vocs']->value['7']);
  }
  if (isset($data['service_links_node_types'])) {
    $data['service_links_node_types']->value['uw_blog'] = 0; /* WAS: 'uw_blog' */
    $data['service_links_node_types']->value['uw_event'] = 0; /* WAS: 'uw_event' */
    $data['service_links_node_types']->value['uw_image_gallery'] = 0; /* WAS: 'uw_image_gallery' */
    $data['service_links_node_types']->value['uw_news_item'] = 0; /* WAS: 'uw_news_item' */
    $data['service_links_node_types']->value['uw_project'] = 0; /* WAS: 'uw_project' */
    $data['service_links_node_types']->value['uw_service'] = 0; /* WAS: 'uw_service' */
    $data['service_links_node_types']->value['uw_web_page'] = 0; /* WAS: 'uw_web_page' */
  }
  if (isset($data['service_links_node_view_modes'])) {
    unset($data['service_links_node_view_modes']->value);
  }
  if (isset($data['xmlsitemap_settings_menu_link_main-menu'])) {
    $data['xmlsitemap_settings_menu_link_main-menu']->value['status'] = 0; /* WAS: 1 */
  }
  if (isset($data['xmlsitemap_settings_node_uw_blog'])) {
    $data['xmlsitemap_settings_node_uw_blog']->value['status'] = 0; /* WAS: 1 */
  }
  if (isset($data['xmlsitemap_settings_node_uw_ct_person_profile'])) {
    $data['xmlsitemap_settings_node_uw_ct_person_profile']->value['status'] = 0; /* WAS: 1 */
  }
  if (isset($data['xmlsitemap_settings_node_uw_event'])) {
    $data['xmlsitemap_settings_node_uw_event']->value['status'] = 0; /* WAS: 1 */
  }
  if (isset($data['xmlsitemap_settings_node_uw_image_gallery'])) {
    $data['xmlsitemap_settings_node_uw_image_gallery']->value['status'] = 0; /* WAS: 1 */
  }
  if (isset($data['xmlsitemap_settings_node_uw_news_item'])) {
    $data['xmlsitemap_settings_node_uw_news_item']->value['status'] = 0; /* WAS: 1 */
  }
  if (isset($data['xmlsitemap_settings_node_uw_special_alert'])) {
    $data['xmlsitemap_settings_node_uw_special_alert']->value['status'] = 1; /* WAS: 0 */
  }
  if (isset($data['xmlsitemap_settings_node_uw_web_form'])) {
    $data['xmlsitemap_settings_node_uw_web_form']->value['status'] = 0; /* WAS: 1 */
  }
  if (isset($data['xmlsitemap_settings_node_uw_web_page'])) {
    $data['xmlsitemap_settings_node_uw_web_page']->value['status'] = 0; /* WAS: 1 */
  }
}
